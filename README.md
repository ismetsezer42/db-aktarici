### DB Aktarıcı

Farklı veritabanında, farklı kolonlarda bulunan verilerin farklı veritabanlarına farklı kolonları doldurarak aktarılmasını kolayca
yapmaktadır.

Verilerini aktarmak için sadece json dosyasını değiştirip onu bu kod ile çalıştırmak yeterlidir

### Bağımlılıklar
 - MySQLdb

 
### Testler
- 30 binden fazla veri üzerinde test edilmiştir
- Lokal bağlantılarda cpu tüketme oranı %1.2 dir: Mem:%0.4


### Örnek

```
{
  "ayarlar":{
    "host":"<host>",
    "username":"<username>",
    "password":"<pass>",
    "aktarilacakdb":"<aktarilacakdb>",
    "eklenecekdb":"<eklenecekdb>"
  },
  "tables" : [
    {
      "aktarilacaktablo" : "membermembership",
      "eklenecektablo" :"kullanici_uyelik_paketi_alanlar",
      "karsilastirma" : [
        {
          "aktarilacakcol" : "0",
          "eklenecekcol" : "id"
        },
        {
          "aktarilacakcol" : "3",
          "eklenecekcol" : "paket_id"
        },
        {
          "aktarilacakcol" : "4",
          "eklenecekcol" : "kullanici_id"
        }
      ]
    }
  ]
}
```
 Buradaki json kodunda, ayarlar kısmında verilerin çekleceği host ve kullanıcı adını giriyor sonrasında
<b>aktarilacakdb</b> kısmına aktarılacak veritabanının adını yazıyoruz. <b> eklenecekdb</b> kısmına da verilerin ekleneceği
veritabanı adını yazıyoruz.

<b>tables</b> kısmına ise her bir tablo çifti için ayrı bir işlem yapıyoruz. Burada <b>aktarilacakcol</b> kısmında aktarılacak veritabanında ki tablonun aktarılacak verisinin kolon numarasını alıyor(0'dan başlayarak) <b> eklenecekcol</b> kısmında ise
ekleyeceğimiz veritabında eklenecek kolonun adını yazıyoruz.Bunları herbiri için yapıyoruz.
Daha sonrasında ise kodumuzu çalıştırıyoruz

```
python dbtable.py dbtable.json
```
Otomatik olarak verileri aktarıcaktır. Birde  json yazarken en son biten itemlere virgül koymuyoruz yoksa hata veriyor. Bunun sebebi de python kodumuzda kullandığımız json loader dan kaynaklıdır.
