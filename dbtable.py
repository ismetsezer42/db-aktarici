# -*- coding: utf-8 -*-
import MySQLdb
import sys
import json
from _mysql_exceptions import IntegrityError,ProgrammingError
reload(sys)

sys.setdefaultencoding("utf-8")

with open(sys.argv[1],'r') as js:
    jsonData = json.load(js)


ayarlar =  jsonData["ayarlar"]
db = MySQLdb.connect(ayarlar["host"],ayarlar["username"],ayarlar["password"],ayarlar["aktarilacakdb"],charset='utf8',use_unicode=True)
veriler = []

cursor = db.cursor()
for table in jsonData["tables"] :
    aktarilacaktablo = table["aktarilacaktablo"]
    eklenecektablo = table["eklenecektablo"]
    sql = "select * from %s" % aktarilacaktablo
    print sql
    try:
        cursor.execute(sql)
        results = cursor.fetchall()

        for row in results:

            count = 0
            temp = []
            for col  in row:

                temp.append(col)
                count+=1
            veriler.append(temp)

    except IndexError as e:
        print e
        print "Verileri Cekilemedi!"

    yeniDbVeri = []
    for veri in veriler:
        temp = []
        for karsilastirma in table["karsilastirma"]:
            temp.append(karsilastirma["eklenecekcol"])
            index = int(karsilastirma["aktarilacakcol"])
            aktarmaverisi = veri[index]
            temp.append(aktarmaverisi)
        yeniDbVeri.append(temp)
    yeniDb = MySQLdb.connect(ayarlar["host"],ayarlar["username"],ayarlar["password"],ayarlar["eklenecekdb"],charset="utf8",use_unicode=True)
    yeniCursor = yeniDb.cursor()



    for yeniveri in yeniDbVeri:
        cols = ""
        datas = ""
        for i in range(len(yeniveri) ):
            if yeniveri[i] == None and type(yeniveri[i]) == "<type 'str'>":
                yeniveri[i] = 'NULL'
            elif yeniveri[i] == None and type(yeniveri[i]) == "<type 'int'>":
                yeniveri[i] = 0
            elif yeniveri[i] == None:
                yeniveri[i] = '0'
                    
            if i % 2 == 0  or i == 0:
                cols += yeniveri[i] + ","
            else:
                try:
                    datas += str(int(yeniveri[i]))+','
                except ValueError as e:
                    yeniveri[i]  = yeniveri[i].replace("'","\"")
                    datas += "'" + yeniveri[i] + "',"
                except TypeError as e:
                    print yeniveri[i]
                    sys.exit(1)
        sql = """insert into {} ({}) values ({}); """.format(eklenecektablo, cols[:-1] , datas[:-1])
        print "Sql Created For New Db"
        print sql
        try:
            print "Executing.."
            yeniCursor.execute(sql)
            yeniDb.commit()
        except ProgrammingError as err:
            print err
            sys.exit(1)
        except IntegrityError as err:
            pass
        except IndexError as e:
            print e
            print "Hata"
            sys.exit(1)
